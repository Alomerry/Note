# PC

## Windows

### WSL2 

#### 设置代理

在 Ubuntu 子系统中，通过 `cat /etc/resolv.conf` 查看 DNS 服务器 IP

```bash
export ALL_PROXY="http://$host_ip:7890"
```

win10 专业工作站 kms 密钥激活步骤

```shell
slmgr /ipk NRG8B-VKK3Q-CXVCJ-9G2XF-6Q84J
slmgr /skms zh.us.to
slmgr /ato
slmgr /xpr
```

### 开放 SSH

- `sudo vi /etc/ssh/sshd_config` 修改 **port**、**permitRootLogin**、**passwordAuthentication**
- `sudo vim /etc/hosts.allow` 添加 `sshd: ALL`
- `sudo service ssh --full-restart`
- 查看 WSL IP 地址
- 在 Windows 下 `netsh interface portproxy add v4tov4 listenaddress=0.0.0.0 listenport=23 connectaddress=<WSL-IP> connectport=<WSL-SSH-PORT>`

https://zhuanlan.zhihu.com/p/357038111

### OBS browser source

无法使用 speech synthesis

window.speechSynthesis.getVoices() 返回空可用语言列表
http://5.9.10.113/65808042/using-speech-synthesis-in-obs-browser-source-chromium-embedded
https://obsproject.com/forum/search/2669636/?q=speechSynthesis&o=date
https://obsproject.com/forum/threads/speechsynthesis-voices.124317/
https://obsproject.com/forum/threads/how-to-get-console-log-in-browsersource.110221/

## Linux

### 查看端口占用

#### lsof

```
# lsof -i:8000
COMMAND   PID USER   FD   TYPE   DEVICE SIZE/OFF NODE NAME
nodejs  26993 root  10u   IPv4 37999514      0t0  TCP *:8000 (LISTEN)
```

- COMMAND 进程名称
- PID 进程标识符
- USER 进程所有者
- FD 文件描述符，应用程序通过文件描述符识别改文件。如 cwd、txt 等
- TYPE 文件类型，如 DIR、REG 等
- DEVICE 指定磁盘名称
- SIZE 文件大小
- NODE 索引节点
- NAME 打开文件的确切名称

#### netstat

```shell
netstat -<option> | grep <port>

- -t (tcp) 仅显示 tcp 相关选项
- -u (udp) 仅显示 udp 相关选项
- -n 拒绝显示别名，能显示数字的全部转化为数字
- -l 仅列出在 Listen（监听）的服务状态
- -p 显示建立相关链接的程序名
```

### 解决 ssh 连接长时间不操作断开连接的问题

通过 ssh 连上服务器后，一段时间不操作，就会自动中断，并报出以下信息：

client_loop: send disconnect: Broken pipe
这带来很大的困扰，过一会就要重新连接，之前的临时环境变量也会丢失。

配置~/.ssh/config文件，增加以下内容即可：

```bash
Host *
        # 断开时重试连接的次数
        ServerAliveCountMax 5

        # 每隔5秒自动发送一个空的请求以保持连接
        ServerAliveInterval 5
```

### nohup 和 &

- nohup
  用途：不挂断运行命令
- &
  用途：后台运行

Ubuntu开放对外端口

1.查看已经开启的端口

sudo ufw status

2.打开80端口

sudo ufw allow 80

3.防火墙开启

sudo ufw enable

4.防火墙重启

sudo ufw reload

#### [shell bash -f -d](https://www.cnblogs.com/emanlee/p/3583769.html)

### 安装 zsh oh-my-zsh

- `sudo apt-get install -y zsh`
- `sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"`
- 从 gitee 安装，`REMOTE=https://gitee.com/mirrors/oh-my-zsh.git sh -c "$(curl -fsSL https://gitee.com/mirrors/oh-my-zsh/raw/master/tools/install.sh)"`
- `sh -c "$(wget -O- https://gitee.com/mirrors/oh-my-zsh/raw/master/tools/install.sh)"`
install [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md) [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md)

### 安装 homebrew

:::: code-group
::: code-group-item install
```shell
export HOMEBREW_BREW_GIT_REMOTE="https://mirrors.ustc.edu.cn/brew.git"
export HOMEBREW_CORE_GIT_REMOTE="https://mirrors.ustc.edu.cn/homebrew-core.git"

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```
:::
::: code-group-item output
```shell
==> Checking for `sudo` access (which may request your password)...
Password:
==> This script will install:
/opt/homebrew/bin/brew
/opt/homebrew/share/doc/homebrew
/opt/homebrew/share/man/man1/brew.1
/opt/homebrew/share/zsh/site-functions/_brew
/opt/homebrew/etc/bash_completion.d/brew
/opt/homebrew
==> The following new directories will be created:
/opt/homebrew/bin
/opt/homebrew/etc
/opt/homebrew/include
/opt/homebrew/lib
/opt/homebrew/sbin
/opt/homebrew/share
/opt/homebrew/var
/opt/homebrew/opt
/opt/homebrew/share/zsh
/opt/homebrew/share/zsh/site-functions
/opt/homebrew/var/homebrew
/opt/homebrew/var/homebrew/linked
/opt/homebrew/Cellar
/opt/homebrew/Caskroom
/opt/homebrew/Frameworks
==> HOMEBREW_BREW_GIT_REMOTE is set to a non-default URL:
https://mirrors.ustc.edu.cn/brew.git will be used as the Homebrew/brew Git remote.
==> HOMEBREW_CORE_GIT_REMOTE is set to a non-default URL:
https://mirrors.ustc.edu.cn/homebrew-core.git will be used as the Homebrew/homebrew-core Git remote.

Press RETURN to continue or any other key to abort:
➜  Downloads /bin/bash install.sh
==> Checking for `sudo` access (which may request your password)...
Password:
==> This script will install:
/opt/homebrew/bin/brew
/opt/homebrew/share/doc/homebrew
/opt/homebrew/share/man/man1/brew.1
/opt/homebrew/share/zsh/site-functions/_brew
/opt/homebrew/etc/bash_completion.d/brew
/opt/homebrew
==> The following new directories will be created:
/opt/homebrew/bin
/opt/homebrew/etc
/opt/homebrew/include
/opt/homebrew/lib
/opt/homebrew/sbin
/opt/homebrew/share
/opt/homebrew/var
/opt/homebrew/opt
/opt/homebrew/share/zsh
/opt/homebrew/share/zsh/site-functions
/opt/homebrew/var/homebrew
/opt/homebrew/var/homebrew/linked
/opt/homebrew/Cellar
/opt/homebrew/Caskroom
/opt/homebrew/Frameworks
==> HOMEBREW_BREW_GIT_REMOTE is set to a non-default URL:
https://mirrors.ustc.edu.cn/brew.git will be used as the Homebrew/brew Git remote.
==> HOMEBREW_CORE_GIT_REMOTE is set to a non-default URL:
https://mirrors.ustc.edu.cn/homebrew-core.git will be used as the Homebrew/homebrew-core Git remote.

Press RETURN to continue or any other key to abort:
==> /usr/bin/sudo /bin/mkdir -p /opt/homebrew
==> /usr/bin/sudo /usr/sbin/chown root:wheel /opt/homebrew
==> /usr/bin/sudo /bin/mkdir -p /opt/homebrew/bin /opt/homebrew/etc /opt/homebrew/include /opt/homebrew/lib /opt/homebrew/sbin /opt/homebrew/share /opt/homebrew/var /opt/homebrew/opt /opt/homebrew/share/zsh /opt/homebrew/share/zsh/site-functions /opt/homebrew/var/homebrew /opt/homebrew/var/homebrew/linked /opt/homebrew/Cellar /opt/homebrew/Caskroom /opt/homebrew/Frameworks
==> /usr/bin/sudo /bin/chmod ug=rwx /opt/homebrew/bin /opt/homebrew/etc /opt/homebrew/include /opt/homebrew/lib /opt/homebrew/sbin /opt/homebrew/share /opt/homebrew/var /opt/homebrew/opt /opt/homebrew/share/zsh /opt/homebrew/share/zsh/site-functions /opt/homebrew/var/homebrew /opt/homebrew/var/homebrew/linked /opt/homebrew/Cellar /opt/homebrew/Caskroom /opt/homebrew/Frameworks
==> /usr/bin/sudo /bin/chmod go-w /opt/homebrew/share/zsh /opt/homebrew/share/zsh/site-functions
==> /usr/bin/sudo /usr/sbin/chown alomerry /opt/homebrew/bin /opt/homebrew/etc /opt/homebrew/include /opt/homebrew/lib /opt/homebrew/sbin /opt/homebrew/share /opt/homebrew/var /opt/homebrew/opt /opt/homebrew/share/zsh /opt/homebrew/share/zsh/site-functions /opt/homebrew/var/homebrew /opt/homebrew/var/homebrew/linked /opt/homebrew/Cellar /opt/homebrew/Caskroom /opt/homebrew/Frameworks
==> /usr/bin/sudo /usr/bin/chgrp admin /opt/homebrew/bin /opt/homebrew/etc /opt/homebrew/include /opt/homebrew/lib /opt/homebrew/sbin /opt/homebrew/share /opt/homebrew/var /opt/homebrew/opt /opt/homebrew/share/zsh /opt/homebrew/share/zsh/site-functions /opt/homebrew/var/homebrew /opt/homebrew/var/homebrew/linked /opt/homebrew/Cellar /opt/homebrew/Caskroom /opt/homebrew/Frameworks
==> /usr/bin/sudo /usr/sbin/chown -R alomerry:admin /opt/homebrew
==> /usr/bin/sudo /bin/mkdir -p /Users/alomerry/Library/Caches/Homebrew
==> /usr/bin/sudo /bin/chmod g+rwx /Users/alomerry/Library/Caches/Homebrew
==> /usr/bin/sudo /usr/sbin/chown -R alomerry /Users/alomerry/Library/Caches/Homebrew
==> Downloading and installing Homebrew...
remote: Enumerating objects: 198638, done.
remote: Counting objects: 100% (198638/198638), done.
remote: Compressing objects: 100% (46213/46213), done.
remote: Total 198638 (delta 147604), reused 198517 (delta 147557)
Receiving objects: 100% (198638/198638), 52.63 MiB | 10.96 MiB/s, done.
Resolving deltas: 100% (147604/147604), done.
From https://mirrors.ustc.edu.cn/brew
 * [new branch]      master     -> origin/master
 * [new tag]             3.3.7      -> 3.3.7
remote: Enumerating objects: 7531, done.
remote: Counting objects: 100% (7531/7531), done.
remote: Compressing objects: 100% (1581/1581), done.
remote: Total 7531 (delta 5798), reused 7531 (delta 5798)
Receiving objects: 100% (7531/7531), 1.61 MiB | 10.80 MiB/s, done.
Resolving deltas: 100% (5798/5798), completed with 737 local objects.
From https://mirrors.ustc.edu.cn/brew
 * [new tag]             1.1.0.1                           -> 1.1.0.1
 * [new tag]             1.1.2.1                           -> 1.1.2.1
 * [new tag]             1.2.7                             -> 1.2.7
 * [new tag]             1.2.8                             -> 1.2.8
 * [new tag]             backup/activesupport-23-38-09     -> backup/activesupport-23-38-09
 * [new tag]             backup/brew-cask-style-14-54-55   -> backup/brew-cask-style-14-54-55
 * [new tag]             backup/create-cache-00-29-47      -> backup/create-cache-00-29-47
 * [new tag]             backup/days-03-02-52              -> backup/days-03-02-52
 * [new tag]             backup/days-03-02-59              -> backup/days-03-02-59
 * [new tag]             backup/days-19-30-23              -> backup/days-19-30-23
 * [new tag]             backup/gpg-verification-01-53-16  -> backup/gpg-verification-01-53-16
 * [new tag]             backup/remove-popen-read-19-56-50 -> backup/remove-popen-read-19-56-50
 * [new tag]             backup/remove-popen-read-20-00-21 -> backup/remove-popen-read-20-00-21
HEAD is now at 5fb34c8ef Merge pull request #12536 from Homebrew/dependabot/bundler/Library/Homebrew/parser-3.0.3.2
==> Tapping homebrew/core
remote: Enumerating objects: 1106160, done.
remote: Total 1106160 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (1106160/1106160), 454.71 MiB | 10.78 MiB/s, done.
Resolving deltas: 100% (769281/769281), done.
From https://mirrors.ustc.edu.cn/homebrew-core
 * [new branch]      master     -> origin/master
HEAD is now at b3e4725d75e podman: update 3.4.4 bottle.
HOMEBREW_BREW_GIT_REMOTE set: using https://mirrors.ustc.edu.cn/brew.git for Homebrew/brew Git remote.
HOMEBREW_CORE_GIT_REMOTE set: using https://mirrors.ustc.edu.cn/homebrew-core.git for Homebrew/core Git remote.
Warning: /opt/homebrew/bin is not in your PATH.
  Instructions on how to configure your shell for Homebrew
  can be found in the 'Next steps' section below.
==> Installation successful!

==> Homebrew has enabled anonymous aggregate formulae and cask analytics.
Read the analytics documentation (and how to opt-out) here:
  https://docs.brew.sh/Analytics
No analytics data has been sent yet (nor will any be during this install run).

==> Homebrew is run entirely by unpaid volunteers. Please consider donating:
  https://github.com/Homebrew/brew#donations

==> Next steps:
- Run these two commands in your terminal to add Homebrew to your PATH:
    echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> /Users/alomerry/.zprofile
    eval "$(/opt/homebrew/bin/brew shellenv)"
- Run these commands in your terminal to add the non-default Git remotes for Homebrew/brew and Homebrew/homebrew-core:
    echo 'export HOMEBREW_BREW_GIT_REMOTE="https://mirrors.ustc.edu.cn/brew.git"' >> /Users/alomerry/.zprofile
    echo 'export HOMEBREW_CORE_GIT_REMOTE="https://mirrors.ustc.edu.cn/homebrew-core.git"' >> /Users/alomerry/.zprofile
    export HOMEBREW_BREW_GIT_REMOTE="https://mirrors.ustc.edu.cn/brew.git"
    export HOMEBREW_CORE_GIT_REMOTE="https://mirrors.ustc.edu.cn/homebrew-core.git"
- Run brew help to get started
- Further documentation:
    https://docs.brew.sh
```
:::
::::

### sudo npm

需要软链

```shell
which npm // 查看 npm 命令所在的位置

sudo ln -s <your node location> /usr/bin/node
sudo ln -s <your npm location> /usr/bin/npm

## VPS

### 迁移

更新 unix 密码 sudo password
生成 ssh-key ssh-keygen
cat /root/.ssh/id_rsa.pub
修改主机名 sudo /etc/hostname sudo reboot
安装宝塔面板 https://www.bt.cn

- 开放端口 修改密码
- 安装 nginx

迁移博客

- 下载 typecho 源码
- 新服务安装 MySQL，并新建同名数据库
- 备份旧数据库，导入新数据库，安装 typecho 并选择使用旧数据
- 替换 usr 文件夹

搭建 v2ray

安装 maven
访问 `https://downloads.apache.org/maven/maven-3/download`
`tar zxvf apache-maven-<version>-bin.tar.gz`
`sudo mv apache-maven-<version>/ /opt/apache-maven-<version>/`

配置环境变量

```shell
sudo vim ~/.bashrc
#如果要配置系统级别的环境变量，则应该编辑以下文件
sudo vim /etc/profile
export M2_HOME=/opt/maven
export M2=$M2_HOME/bin
export PATH=$M2:$PATH
刷新环境变量
source ~/.bashrc
```

安装 jdk
sudo apt-get install openjdk-8-jdk
export M2_HOME=/opt/maven/apache-maven-3.6.3
export CLASSPATH=$CLASSPATH:$M2_HOME/lib
export PATH=$PATH:$M2_HOME/bin
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export JRE_HOME=$JAVA_HOME/jre
export CLASSPATH=$JAVA_HOME/lib:$JRE_HOME/lib:$CLASSPATH
export PATH=$JAVA_HOME/bin:$JRE_HOME/bin:$PATH

ps -aux | grep spring-boot:run

## Examples

[1](https://www.jb51.net/article/57972.htm)
[2](http://c.biancheng.net/view/4028.html)