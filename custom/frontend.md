# 前端

https://github.com/nvm-sh/nvm#install-script

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash

## VuePress 设置

### Markdown 设置

#### 行高亮

```typescript{1,6-8}
import type { UserConfig } from '@vuepress/cli'

export const config: UserConfig = {
  title: '你好， VuePress',

  themeConfig: {
    logo: 'https://vuejs.org/images/logo.png',
  },
}
```

#### 自定义容器

- detail

::: details 点击查看代码
```js
console.log('你好，VuePress！')
```
:::

- Code Group

:::: code-group
::: code-group-item FOO
```js
const foo = 'foo'
```
:::
::: code-group-item BAR
```js
const bar = 'bar'
```
:::
::::

#### 组件

可以在 Markdown 中直接使用 Vue 组件。

```txt
这是默认主题内置的 `<Badge />` 组件 <Badge text="演示" />
```

这是默认主题内置的 `<Badge />` 组件 <Badge text="演示" />

#### Emoji :tada:

输入 `:EMOJICODE:` 来添加 Emoji 表情

## node

### 国内 npm/yarn 安装包很慢

国外地址换成国内地址

```shell
npm config set registry http://registry.npm.taobao.org
yarn config set registry https://registry.npm.taobao.org
```

