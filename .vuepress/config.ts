import {DefaultThemeOptions, defineUserConfig} from 'vuepress'

export default defineUserConfig<DefaultThemeOptions>({
    lang: 'zh-cn',
    title: "Alomerry's Note",
    description: "Alomerry's Note",
    themeConfig: {
        home: '',
        navbar: [],
        logo: 'https://vuejs.org/images/logo.png',
        darkMode: true,
        lastUpdated: true,
        lastUpdatedText: 'Last Updated',
        nextLinks: true,
        prevLinks: true,
        repo: 'https://gitlab.com/Alomerry/Note/',
        repoLabel: '查看源码',
        contributors: true,
        contributorsText: "贡献者",
        smoothScroll: true,
        sidebarDepth: 5,
        tip: 'TIP',
        warning: '警告',
        danger: '危险',
        sidebar: [
            {
                text: '算法解析',
                children: [
                    '/algorithm/leetcode.md',
                    '/algorithm/pat.md',
                    '/algorithm/note.md',
                ],
            },
            {
                text: '博客文章',
                children: [
                    '/blog/blog.md',
                ],
            },
            {
                text: '经典书籍',
                children: [
                    '/books/go-design.md',
                    '/books/gopl.md',
                    // '/books/csapp.md',
                    '/books/gopl.md',
                    '/books/go-std-lib.md'
                ],
            },
            {
                text: '语言心得',
                children: [
                    '/code/golang.md',
                    '/code/java.md',
                ],
            },
            {
                text: '其它笔记',
                children: [
                    '/custom/codeStyle.md',
                    '/custom/githubs.md',
                    '/custom/pc.md',
                    '/custom/frontend.md',
                    '/custom/note.md',
                    '/custom/words.md',
                ],
            },
            {
                text: '文档工具',
                children: [
                    '/doc/docker.md',
                    '/doc/git.md',
                    '/doc/grpc.md',
                    '/doc/mongodb.md',
                    '/doc/qmgo.md',
                    '/doc/redis.md',
                    '/doc/RocketMQ.md',
                    '/doc/spring.md',
                ],
            },
        ],
    },
    extendsMarkdown: (md) => {
        md.set({
            breaks: true,
            linkify: true,
        });
        md.use(require('markdown-it-task-lists'));
    },
})
